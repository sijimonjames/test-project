*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${BROWSER}        Chrome

*** Test Cases ***
Test Web Page
  [Documentation]    Script opens browser and launch www.ranorex.com
   Launch Application

*** Keywords ***
Launch Application
    Open Browser    https://www.ranorex.com    ${BROWSER}
    Wait Until Page Contains    Test Automation for All
    Close Browser